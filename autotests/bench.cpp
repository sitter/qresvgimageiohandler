// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

#include <QDebug>
#include <QDirIterator>
#include <QIcon>
#include <QImageIOHandler>
#include <QPainter>
#include <QSvgRenderer>
#include <QTest>
#include <QUrl>
#include <ResvgQt.h>

using namespace Qt::StringLiterals;

class Bench : public QObject
{
    Q_OBJECT
    struct Data{
        QString path;
        QByteArray data;
    };
    std::vector<Data> datas;
private Q_SLOTS:
    void initTestCase()
    {
        QDirIterator it("/usr/share/icons/breeze", QDir::Files, QDirIterator::Subdirectories);
        while (it.hasNext()) {
            const auto path = it.next();
            if (!path.endsWith(".svg") && !path.endsWith(".svgz")) {
                continue;
            }
            QFile f(path);
            QVERIFY(f.open(QFile::ReadOnly));
            auto data = f.readAll();
            datas.push_back({path, data});
        }
        QImage image;
    }

    void qbench()
    {
        QBENCHMARK {
            for (const auto &data : datas) {
                QSvgRenderer r(data.data);
                auto finalSize = r.defaultSize();
                QImage image(finalSize.width(), finalSize.height(), QImage::Format_ARGB32_Premultiplied);
                image.fill(Qt::transparent);
                QPainter paint(&image);
                r.render(&paint);
            }
        }
    }
    void rbench()
    {
        QBENCHMARK {
            for (const auto &data : datas) {
                ResvgRenderer(data.data, ResvgOptions()).renderToImage();
            }
        }
    }
};

QTEST_MAIN(Bench)

#include "bench.moc"
