// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

#include <QImageIOPlugin>

#include "handler.h"

using namespace Qt::StringLiterals;

class ResvgImageIOPlugin : public QImageIOPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QImageIOHandlerFactoryInterface" FILE "plugin.json")
public:
    Capabilities capabilities(QIODevice *device, const QByteArray &format) const override
    {
        if (format == "svg"_ba || format == "svgz"_ba) {
            return CanRead;
        }
        if (!format.isEmpty()) {
            return {};
        }
        if (device->isReadable()) {
            ResvgImageIOHandler handler;
            handler.setDevice(device);
            return handler.canRead() ? CanRead : Capability();
        }
        return {};
    }

    QImageIOHandler *create(QIODevice *device, const QByteArray &format = QByteArray()) const override
    {
        auto handler = new ResvgImageIOHandler();
        handler->setDevice(device);
        handler->setFormat(format);
        return handler;
    }
};

#include "main.moc"
