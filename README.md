<!--
    SPDX-License-Identifier: CC0-1.0
    SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>
-->

# Better SVG for Qt

To support this properly Qt would need to grow a system whereby we can override libqsvg (e.g. some sort of priority
metadata or something to mark a plugin as preferred).

Based on the c bindings of https://github.com/RazrFalcon/resvg

# Installation

This only works for Qt 6! Meaning you'll want to try this on KDE neon unstable or similar.

First rust up to build resvg https://rustup.rs/ If you set it up correctly you should see the cargo version by running
`cargo version`.

Secondly you need to make sure build dependencies are installed. On neon that is mostly

```bash
sudo apt install qt6-base-dev cmake build-essential kf6-extra-cmake-modules
```

Then you can build it like so

```bash
git clone https://invent.kde.org/sitter/qresvgimageiohandler
cmake -S qresvgimageiohandler -B qresvgimageiohandler/build -DBUILD_WITH_QT6=ON
cmake --build qresvgimageiohandler/build
# WARNING: This replaces your system libqsvg. Be sure to know how to restore it on your system (on neon sudo apt install --reinstall qt6-svg)
sudo cmake --install qresvgimageiohandler/build
```

# Performance

Curiously enough resvg not only seems more correct but also renders breeze-icons faster than qsvg

```
********* Start testing of Bench *********
Config: Using QtTest library 6.6.0, Qt 6.6.0 (x86_64-little_endian-lp64 shared (dynamic) release build; by GCC 11.3.0), neon 22.04
PASS   : Bench::initTestCase()
        PASS   : Bench::qbench()
RESULT : Bench::qbench():
     1,376.2 msecs per iteration (total: 12,386, iterations: 9)
PASS   : Bench::rbench()
RESULT : Bench::rbench():
     1,288.7 msecs per iteration (total: 11,599, iterations: 9)
PASS   : Bench::cleanupTestCase()
Totals: 4 passed, 0 failed, 0 skipped, 0 blacklisted, 27444ms
********* Finished testing of Bench *********
```
