// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

#pragma once

#include <QImageIOHandler>

#include <ResvgQt.h>

class ResvgImageIOHandler : public QImageIOHandler
{
public:
    bool canRead() const override;
    bool read(QImage *image) override;
    bool supportsOption(ImageOption option) const override;
    void setOption(ImageOption option, const QVariant &value) override;
    QVariant option(ImageOption option) const override;
    // TODO need to implement more stuff so we can render the correct scale and what not

private:
    void load() const; // const so it can be called from option()
    mutable ResvgRenderer m_renderer;
    QSize m_targetSize;
};
