// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

#include "handler.h"

using namespace Qt::StringLiterals;

namespace
{
struct IsSvg {
    bool is;
    bool compressed;
};

// Borrowed from Qt and adjusted to be slightly less oldschool
// Copyright (C) 2016 The Qt Company Ltd.
// SPDX-License-Identifier: LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only
[[nodiscard]] IsSvg isPossiblySvg(QIODevice *device)
{
    std::array<char, 64> buf{};
    const qint64 readLen = device->peek(buf.data(), buf.size());
    if (readLen < 8) {
        return {.is = false, .compressed = false};
    }
    if (quint8(buf[0]) == 0x1f && quint8(buf[1]) == 0x8b) {
        return {.is = true, .compressed = true};
    }
    QTextStream str(QByteArray::fromRawData(buf.data(), readLen));
    QByteArray ba = str.read(16).trimmed().toLatin1();
    const bool isSvg =
        ba.startsWith("<?xml") || ba.startsWith("<svg") || ba.startsWith("<!--") || ba.startsWith("<!DOCTYPE svg");
    return {.is = isSvg, .compressed = false};
}
} // namespace

bool ResvgImageIOHandler::canRead() const
{
    if (!device()) {
        return false;
    }

    if (auto isSvg = isPossiblySvg(device()); isSvg.is) {
        setFormat(isSvg.compressed ? "svgz" : "svg");
        return true;
    }
    return false;
}

bool ResvgImageIOHandler::read(QImage *image)
{
    load();
    *image = m_renderer.renderToImage(m_targetSize);
    return !image->isNull();
}

bool ResvgImageIOHandler::supportsOption(ImageOption option) const
{
    switch(option)
    {
    case Size:
    case ScaledSize:
        return true;
    // TODO we could support background if upstream resvg adds the ability to fill the image using different colors
    default: // this is an explicit opt in list, that makes use of default fine
        break;
    }
    return QImageIOHandler::supportsOption(option);
}

void ResvgImageIOHandler::setOption(ImageOption option, const QVariant &value)
{
    switch (option) {
    case Size:
        Q_ASSERT(false); // This cannot be set!
    case ScaledSize:
        m_targetSize = value.toSize();
    default: // this is an explicit opt-in list, that makes use of default fine
        break;
    }
    return QImageIOHandler::setOption(option, value);
}

QVariant ResvgImageIOHandler::option(ImageOption option) const
{
    switch (option) {
    case Size:
        load();
        return m_renderer.defaultSize();
    case ScaledSize:
        return m_targetSize;
    default: // this is an explicit opt-in list, that makes use of default fine
        break;
    }
    return QImageIOHandler::option(option);
}

void ResvgImageIOHandler::load() const
{
    if (m_renderer.isValid()) {
        return;
    }

    ResvgOptions options;
    if (!m_renderer.load(device()->readAll(), options)) {
        qWarning() << "failed to load";
    }
}
